/**
 * Created by rashmi on 23/2/16.
 */
Verification = new FS.Collection('verification',
    {stores:[new FS.Store.FileSystem('verification',{})]
    });
Verification.allow({
    insert:function(){
        return true;
    },
    update:function(){
        return true;
    },
    remove:function(){
        return true;
    },
    download:function(){
        return true;
    }
})
