/**
 * Created by rashmi on 17/2/16.
 */
Notifications = new Meteor.Collection('notifications');

Notifications.allow({
    insert: function(){
        return true;
    },
    update: function(){
        return true;
    }
});