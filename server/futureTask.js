/**
 * Created by rashmi on 17/2/16.
 */

FutureTasks = new Meteor.Collection('future_tasks'); // server-side only
FutureTasks.allow({
    insert: function(){
        return true;
    },
    update: function(){
        return true;
    },
    remove: function(){
        return true;
    }
});