/*Meteor.publishComposite("items", function() {
  return {
    find: function() {
      return Items.find({});
    }

  }
});*/
Meteor.publish('projects',function(userId){
      return Projects.find({$or:[{invited:this.userId},{userId:this.userId}]});
});
Meteor.publish('discussions',function(){
  return Discussions.find({});
});

Meteor.publish('issues', function () {
  return Issues.find({});
});

 //ReactiveTable.publish('issues', function (project) { return Issues.find({project:project}); }, {});
ReactiveTable.publish('comments', function (project) { return Comments; }, {});



Meteor.publish('uploads',function(project){
  return Uploads.find({project:project});
});
Meteor.publish('comments',function(){
  return Comments.find({});
});
Meteor.publish('conversations',function(){
  return Conversations.find({});
});
Meteor.publish('calevents',function(project){
  return Calevents.find({project:project});
});
Meteor.publish('calevent',function(){
  return Calevents.find({});
});

Meteor.publish('todos',function(project){
  return Todos.find({project:project});
});

Meteor.publish("chatrooms",function(){
    return ChatRooms.find({});
});
Meteor.publish("onlusers",function(){
    return Meteor.users.find({"status.online":true},{username:1});
});
Meteor.publish('directory',function(){
  //console.log(Meteor.users.find({},{}));
  return Meteor.users.find({},{});
});
Meteor.publish('invites',function(){
  return Invites.find({},{});
});

Meteor.publish(null,function(){
  return Meteor.roles.find({});
});
Meteor.publish('verification',function(project){
  return Verification.find({project:project});
});
Meteor.publish('archives', function(project){
  return [

  Todos.find({project:project,archived:true}),
  Projects.find({project:project,archived:true})
  ];
});
