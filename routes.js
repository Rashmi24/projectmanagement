Router.map(function() {

  this.route('home', {
    path: '/',
    layoutTemplate:'nosidebar'
  });

  this.route('archives', {
    layoutTemplate: 'mainLayout',
    path: '/archives',
    loginRequired: 'entrySignIn',
    waitOn: function () {
      Meteor.subscribe('archives', Session.get('active_project'));

    }
  });
  this.route('roles', {
    path: '/roles',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){
      return Meteor.subscribe('directory');
    }
  });
this.route('issuetracker', {
    path: '/issuetracker',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){
	Meteor.subscribe('directory');
      Meteor.subscribe('invites');

     return Meteor.subscribe('issues');
    }
  });
this.route('conversations', {
    path: '/conversations',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){
	
Meteor.subscribe('directory');
Meteor.subscribe('projects',this.params.id);
//return Meteor.subscribe('comments');
     return Meteor.subscribe('conversations');
    }
  });
this.route('messagechat', {
    path: '/messagechat',
   
    loginRequired: 'entrySignIn',
   /* waitOn:function(){
	return Meteor.subscribe("chatrooms");
 return Meteor.subscribe("onlusers");

    
    } */
  });
this.route('discussions',{
path:'/discussions',
layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
waitOn:function(){
Meteor.subscribe('projects',this.params.id);
Meteor.subscribe('discussions');
return Meteor.subscribe('comments');
}
});
  this.route('calendar', {
    path: '/calendar',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){
      return Meteor.subscribe('calevent');
    }
  });
  this.route('dashboard', {
    path: '/dashboard',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){
      Meteor.subscribe('directory');
      Meteor.subscribe('invites');

      return Meteor.subscribe('projects',Meteor.userId());
    },
    data:{
      'projects':function(){
        return Projects.find();
      }
    },
    onAfterAction: function() {

    }
  });
  this.route('timeline', {
    path: '/projects/:id/timeline',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){

      Meteor.subscribe('todos',this.params.id);

      Meteor.subscribe('directory');
      Meteor.subscribe('invites');

      return Meteor.subscribe('projects',Meteor.userId());
    },
    data:{
      'projects':function(){

        return Projects.findOne({_id:Session.get('active_project')});
      }
    },
    onAfterAction: function() {

    }
  });
  this.route('projectView',{
    path:'/projects/:id',
    layoutTemplate:'mainLayout',
    loginRequired:'entrySignIn',
    waitOn:function(){

      Meteor.subscribe('conversations',this.params.id);
      Meteor.subscribe('todos',this.params.id);
      Meteor.subscribe('calevents',this.params.id);
      Meteor.subscribe('verification',this.params.id);
      Meteor.subscribe('directory');
      Meteor.subscribe('invites');

      return Meteor.subscribe('projects');
    },
    data:function(){
      Session.set('active_project',this.params.id);
      return Projects.findOne({_id:this.params.id});
    },
    onAfterAction:function(){
      SEO.set({
        title:'Project View | ' + SEO.settings.title
      })
    }
  });
  this.route('myuploads', {
    path: '/projects/:id/myuploads',
    layoutTemplate:'mainLayout',
    loginRequired: 'entrySignIn',
    waitOn:function(){

      Meteor.subscribe('todos',this.params.id);
      Meteor.subscribe('verification');
      Meteor.subscribe('directory');
      Meteor.subscribe('invites');

      return Meteor.subscribe('projects',Meteor.userId());
    },
    data:{
      'projects':function(){

        return Projects.findOne({_id:Session.get('active_project')});
      }
    },
    onAfterAction: function() {

    }
  });


  /*this.route('notFound', {
    path: '*',
    where: 'server',
    action: function() {
      this.response.statusCode = 404;
      this.response.end(Handlebars.templates['404']());
    }
  });*/

});
