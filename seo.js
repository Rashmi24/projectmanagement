Meteor.startup(function() {
  if(Meteor.isClient) {
    SEO.config({
      title: 'Project Management App',
      meta: {
        'description': 'Project Management Made Easy'
      },
      
      ignore: {
        meta: ['fragment', 'viewport', 'msapplication-TileColor', 'msapplication-TileImage', 'msapplication-config'],
        link: ['stylesheet', 'apple-touch-icon', 'rel', 'shortcut icon', 'icon']
      }
    });
  }
});
