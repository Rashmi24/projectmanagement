Meteor.startup(function() {
   /* Accounts.ui.config({
        passwordSignupFields: 'USERNAME_AND_EMAIL',
        homeRoute: '/',
        dashboardRoute: '/dashboard',
        profileRoute: '/profile',
        language: 'en',
        showSignupCode: false,
        requestPermissions: {},
        extraSignUpFields: [{

            fieldName: 'name',
            fieldLabel: 'First name',
            inputType: 'text',
            visible: true,
            placeholder: "",


            required: true
        }]
    });
*/
    AccountsEntry.config({
        passwordSignupFields: 'USERNAME_AND_EMAIL',
        homeRoute: '/',
        dashboardRoute: '/dashboard',
        profileRoute: '/profile',
        language: 'en',
        showSignupCode: false,
        /*extraSignUpFields: [{
            field: "name",

            placeholder: "Enter your name",
            label: "Your Name",
            type: "text",
            required: true
        }]*/
    });
});
