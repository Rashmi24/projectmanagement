/**
 * Created by rashmi on 22/2/16.
 */
Template.uploadScreenShot.events({
    'click .closeUpload':function(){
        Session.set('attachScreenShots',false);
    },
    'click .saveUpload':function(evt,tmpl){
        var project=Session.get('active_project');
        var theFile=Session.get('theFile');

        var taskOwner=Session.get('taskOwner');



        Session.set('attachScreenShots',false);
    },
    'change .myUpload':function(evt,tmpl){


            FS.Utility.eachFile(evt,function(file){

            var theFile = new FS.File(file);
            theFile.fileName=theFile.name();
            console.log(theFile.fileName);


            var project=Session.get('active_project');
            theFile.creatorId = Meteor.userId();
            theFile.project = Session.get('active_project');
            theFile.taskId=Session.get('TaskId');
            console.log(theFile.taskId);
            theFile.taskName=Todos.findOne({_id:theFile.taskId},{note:1}).note;

            var taskOwner=Todos.findOne({_id:theFile.taskId},{targetowner:1}).targetowner;

            var guide=Invites.findOne({projectid:project},{role:"guide"}).targetname;
            var guideid=Invites.findOne({projectid:project,role:"guide"}).target;
            theFile.taskOwner=taskOwner;
                theFile.taskOwnerName=Meteor.users.findOne({_id:taskOwner},{profile:{name:1}}).profile.name;
                    theFile.guide=guide;
            theFile.guideid=guideid;
            if(theFile.creatorId==taskOwner){
                Verification.insert(theFile,function(err,fileObj){
                    if(!err){
                        console.log("inside err");

                        var attachments=[];
                        var  attObject = {};
                        attObject.fileName=theFile.fileName;
                        attObject.filePath=".meteor/local/cfs/files/verification/"+theFile.fileName;
                        attachments.push(attObject);
                        Meteor.call('sendEmailVerify',taskOwner,attachments,guideid,guide);
                        GlobalNotification.success({
                            // title: 'New Project Created',
                            content: "Verification document has been sent through email to guie" +guide ,
                            duration: 5 // duration the notification should stay in seconds
                        });
                        console.log("Email sent");
                    }
                })
            }
            else{
                alert("Sorry You Are Not The Owner Of This Task");
                Meteor.call('completeTodo',theFile.taskId,false);
                Session.set('attachScreenShots',false)
            }


        });
       // console.log("after FS");
        //alert("saved");
    }
});
Template.uploadScreenShot.helpers({
    'uploads':function(){
        console.log("inside uploads helper");
        console.log(Verification.find({project:Session.get('active_project'),taskId:Session.get('taskId')}).fetch());
        console.log(Verification.find({project:Session.get('active_project'),taskId:Session.get('taskId')}).count());
        return Verification.find({project:Session.get('active_project'),taskId:Session.get('taskId'),creatorId:Meteor.userId()}).fetch();
    }
});