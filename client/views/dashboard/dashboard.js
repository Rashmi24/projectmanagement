Template.dashboard.rendered = function() {

};
Template.dashboard.events({
  'keyup input[type=text]':function(event,tmpl){
    if(event.which === 27 || event.which === 13){
      event.preventDefault();
      var project = {};
      project.name = tmpl.find('#projectNameEnter').value;
      project.ownername=Meteor.users.findOne({_id:Meteor.userId()},{profile:{name:1}}).profile.name;
      console.log(project.ownername);
      project.date=moment().calendar();
      console.log(project.date);
      //project.ownername=Meteor.user().findOne({_id:project.userId}).profile.name;
      //console.log(project.ownername);
      Meteor.call('saveProject',project);
      Session.set('newProject',true);
      GlobalNotification.success({
          // title: 'New Project Created',
        content: project.ownername +"created new project "+project.name,
        duration: 5 // duration the notification should stay in seconds
      });
      console.log(Session.get('newProject'));
    }
  },
  'click .createProject':function(event,tmpl){
    console.log("Inside Button Clicked");
    Session.set('ProjectInfo',true);
  },
  'click .deleteConfirmation':function(evt,tmpl){
    evt.preventDefault();
    evt.stopPropagation();
    Session.set('projectToDelete',this._id);
  },
  'click .cancelDelete':function(){
    return Session.set('projectToDelete',null);
  },
  'click .requestBox':function(){
    console.log("Inside request event");
    Session.set('my_invites',true);
  }
});
Template.ProjectInfo.events({
  'click .closeProject':function(event,tmpl){
    Session.set('ProjectInfo',false);
  },
  'click .saveProject':function(event,tmpl){
    var project={};
    project.name=tmpl.find('#createProject').value;
    project.ownername=Meteor.users.findOne({_id:Meteor.userId()},{profile:{name:1}}).profile.name;
    console.log(project.ownername);
   // project.date=moment().calendar();
    project.endDate=tmpl.find('#ProjectEndDate').value;
	project.date=moment().calendar();
    project.guide=tmpl.find('#guideToBeInvited').value
    console.log(project.guide);

    project.guidename=Meteor.users.findOne({_id:project.guide},{profile:{name:1}}).profile.name;
    console.log(project.guidename);

   var id=Meteor.call('saveProject',project);

    Session.set('newProject',true);
    GlobalNotification.success({
      // title: 'New Project Created',
      content: project.ownername +"created new project "+project.name,
      duration: 5 // duration the notification should stay in seconds
    });
    Session.set('ProjectInfo',false);

    console.log(Session.get('newProject'));
  }


});
Template.ProjectInfo.rendered = function(){
  $('.datetimepicker').datetimepicker();
}
Template.ProjectInfo.helpers({
  guideList:function(){

    return Meteor.users.find({},{profile:{name:1}});
  }

})
Template.dashboard.helpers({
  projectToDelete:function(){
    return Session.get('projectToDelete');
  },
  my_invites:function(){
    return Session.get('my_invites');
  },
  projectInfo:function(){
    return Session.get('ProjectInfo');
  },
  requestCount:function(){
    var count = Invites.find({target:Meteor.userId(),live:1,reciprocal:0}).count();
    if(count >= 10){
      return '10+';
    } else {
      return count;
    }
  }
 /* 'createdByMe':function(){
    var me=Meteor.userId();
    var projectid=Session.get('addedToProject');
    console.log(projectid);
    var myid=Projects.findOne({_id:projectid}).userId;
    if(me===myid){
      Session.set('createdByMe',true);
    }
    else{
      Session.set('createdByOthers',true);
    }
      return Session.get('createdByMe');
  },
  'createdByOthers':function(){
   return Session.get('createdByOthers');
  }*/
})

Template.delconfirm.events({
  'click .deleteConfirmed':function(evt,tmpl){
    var projectid=Session.get('projectToDelete');
    var projectname=Projects.findOne({_id:projectid},{name:1}).name;
    console.log(projectname);
    Meteor.call('removeProject',Session.get('projectToDelete'));

    GlobalNotification.success({
      // title: 'New Project Created',
      content: "Project " +projectname + "is deleted" ,
      duration: 5 // duration the notification should stay in seconds
    });

    Session.set('projectToDelete',null);
  }
})
Template.projectView.helpers({
  editing_calevent:function(){
    return Session.get('editing_calevent');
  },
  adding_conversation:function(){
    return Session.get('adding_conversation');
  },
  adding_todo:function(){
    return Session.get('adding_todo');
  },
  email_invite:function(){
    return Session.get('email_invite')
  },
  see_members:function(){
    return Session.get('see_members');
  },
  attachScreenShots:function(){
    return Session.get('attachScreenShots')
  }
});
Template.showRequest.events({
  'click .closeRequest':function(){
    Session.set('my_invites',false);
  },
  'click .acceptrequest':function(){

    console.log("Inside accept request");
    console.log(this._id);
    var id=this._id;
    var me=Meteor.userId();
    console.log(me);
  /*  var me=id.me;
    console.log(me);*/

    var target=Meteor.userId();
    var projectid=Invites.findOne({_id:id}).projectid;
    //var projectname=Projects.findOne({_id:projectid},{name:1}).name;

    console.log(projectid);
    //var reciprocal=1;
    console.log(target);
    Meteor.call('updateInvites',target,id,projectid);
    Session.set('addedToProject',projectid);
    Session.set('newMember',true);
    GlobalNotification.success({
      // title: 'New Project Created',
      content: "You have accepted to be a part of new project!" ,
      duration: 5 // duration the notification should stay in seconds
    });
    console.log("request accepted");
    console.log(Invites.find({reciprocal:1,target:Meteor.userId(),live:1},{}).count());
    console.log(Invites.find({},{}).count());
  },
  'click .denyrequest':function(){
    var id=this._id;
    //var project=id.projectid;
    //console.log(projectid);
    var target=Meteor.userId();
    Meteor.call('deleteinvite',target,id);
    console.log("request denied")
  }
});
Template.showRequest.helpers({
  'requestForMe':function(){
  console.log("inside requestForMe");

  console.log(Invites.find({target:Meteor.userId(),live:1,reciprocal:0}).count());
  return Invites.find({target:Meteor.userId(),live:1,reciprocal:0});

}
});
