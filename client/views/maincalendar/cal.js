




Template.dialog.events({
'click .closeDialog' : function(event,template){
Session.set('editing_event',null);
},
'click .updateTitle':function(evt,tmpl)
{
var title=tmpl.find('#title').value;
Meteor.call('updateTitle',Session.get('editing_event'),title);
Session.set('editing_event',null);
},
'click .removeEvent':function(evt,tmpl)
{
Meteor.call('removeEvent',Session.get('editing_event'));
Session.set('editing_event',null);
}
});



Template.main.helpers({
editing_event:function(){
return Session.get('editing_event');
}
});
Template.dialog.helpers({
title:function()
{
var ce=CalEvent.findOne({_id:Session.get('editing_event')});
return ce.title;
}
});

Template.dialog.rendered= function(){
if(this.params.id)
{
var calevent = CalEvent.findOne({_id:this.params.id});
if(calevent)
{
$('#title').val(calevent.title);
}
}
}
 Template.main.rendered = function(){
var calendar=$('#calendar').fullCalendar({
dayClick:function(date,allday,jsEvent,view){
var ce={};
ce.start=date;
ce.end=date;
ce.title='New Event';
ce.owner=Meteor.userId();
Meteor.call('saveCalEvent',ce);
},
eventClick:function(calEvent,jsEvent,view){
Session.set('editing_event',calEvent._id);
$('#title').val(calEvent.title);
},
eventDrop:function(reqEvent)
{
Meteor.call('moveEvent',reqEvent);
},
events:function(start,end,callback)
{
var calEvents=CalEvent.find({},{reactive:false}).fetch();
callback(calEvents);
},
editable:true,
selectable:true

}).data().fullCalendar;
/*Deps.autorun(function(){
CalEvent.find().fetch();
if(calendar){
calendar.refetchEvents();
}
})*/
}
}


