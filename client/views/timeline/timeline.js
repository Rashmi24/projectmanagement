/**
 * Created by rashmi on 19/2/16.
 */
Template.timeline.helpers({
    /*'newProject':function(){
       return  Session.get('newProject');
    },*/

    'project':function(){
        var project=Session.get('active_project');
        return Projects.find({_id:project});
    },
    /*'newTask':function(){
        return Session.get('newTask');
    },*/
    'task':function(){
       return Todos.find({project:Session.get('active_project')}).fetch();
    },
    /*'newMember':function(){
        return Session.get('newMember');
    },*/
    'invite':function(){
        var project=Session.get('active_project');
        return Invites.find({me:Meteor.userId(),projectid:project,live:1,reciprocal:0}).fetch();
    },
   /* 'addedToProject':function(){
        return Session.get('addedToProject');
    },*/
    'accepted':function(){
        var project=Session.get('addedToProject');
       // Invites.find({reciprocal:1,live:1,projectid:project}).fetch()
        return Invites.find({projectid:project,live:1,reciprocal:1}).fetch();
    },
    'todos':function(){
        var project=Session.get('active_project');
        return Todos.find({project:project,completed:true});
    }

});