

Template.invited.events({
  'click .emailInvite': function(event, template){
    console.log("inside emailinvite");
     Session.set('email_invite',true);
     console.log(Session.get('email_invite'));
  },
  'click .seeInvites':function(event,template){
    Session.set('see_invites',true);
    console.log(Session.get('see_invites'));
  },
  'change .membersInProject':function(event,template){
    var id=this._id;

    var project=Session.get('active_project');
    console.log(id);
    var  role=event.currentTarget.value;
    console.log(role);

    Meteor.call('addRolesToInvites',id,project,role);
    console.log(Invites.findOne({_id:id},{role:1}).role);
    //console.log(role);

  },


  'click .inviteUser':function(event,template){


    var project=Session.get('active_project');
    var invites={};

    invites.me=Meteor.userId();

    invites.live=1;
    invites.target=template.find('#userToBeInvited').value;//Id of user to be added
    console.log(invites.target);
    invites.targetname=Meteor.users.findOne({_id:invites.target},{profile:{name:1}}).profile.name;
    console.log(invites.targetname);
    invites.reciprocal=0;
    invites.projectid=project;
    invites.role="";
    invites.projectname=Projects.findOne({_id:invites.projectid},{name:1}).name;

    invites.myname=Meteor.users.findOne({_id:Meteor.userId()},{profile:{name:1}}).profile.name;
    console.log(invites.date);
    console.log(project);


    Meteor.call('inviteUser',invites);
    GlobalNotification.success({
      // title: 'New Project Created',
      content: "Request has been sent to "+invites.targetname+" to join your project" ,
      duration: 5 // duration the notification should stay in seconds
    });
    Session.set('newMember',true);
   // console.log(Invites.findOne({projectid:project},{_id:1}));
    console.log('Done Inserting');
    console.log(Invites.find({reciprocal:1,me:Meteor.userId(),projectid:project,live:1},{}).count());
    //console.log(Invites.findOne({me:Meteor.userId(),projectid:project,live:1,reciprocal:0},{_id:1}));

  },

  'click .seeInvitedMembers':function(){
    Session.set('see_members',true);
  },
  'click .removeUser':function(){
    var projectowner=Projects.findOne({_id:Session.get('active_project')}).userId;
    console.log(projectowner);
    var id=this._id;

   // var membername=Meteor.users.findOne({_id:id},{profile:{name:1}}).profile.name;
    console.log(id);
    var me=Meteor.userId();
    console.log(Invites.find({},{}).count());
    if(projectowner===me){
      Meteor.call('removeUser',id);
      Session.set('removeUserId',id);
      Session.set('removeUser',true);
      //Session.set('removedUser',membername);
      console.log("done removing user");
      console.log(Invites.find({},{}).count());
    }
    else{
      alert("SORRY! You cannot remove this member");
    }


  }
});


Template.invited.helpers({
  'userList': function(){

    return Meteor.users.find({},{profile:{name:1}});
  },
  'invitedMembers':function(){
    var project=Session.get('active_project');
    console.log(project);
    var id=Meteor.userId();

   /* if(Projects.findOne({_id:project}).userId==id){
      var invites={};
      invites.me=id;
      invites.target=id;
      invites.targetname=Meteor.users.findOne({_id:invites.target},{profile:{name:1}}).profile.name;
      invites.projectid=project;
      invites.live=1;
      invites.reciprocal=1;
      invites.role="leader";
      Meteor.call('inviteUser',invites);
    }*/

    //return Invites.find({$or:[{target:Meteor.userId()},{me:Meteor.userId()}]},{$and:[{reciprocal:1},{live:1},{projectid:project}]});
   return {invites:Invites.find({reciprocal:1,live:1,projectid:project}).fetch(),
          owner:Projects.findOne({_id:project},{ownername:1})};
  }/*,
  'isOwner':function(){
    var id=Meteor.userId();
    var project=Session.get('active_project');
    var projectowner=Projects.findOne({_id:project},{userId:1}).userId;
    if(id==projectowner){
      return true;
    }
    else
      return false;
  }
*/




});






Template.sendInvite.events({
  'click .sendEmail': function(event, tmpl){
    console.log("inside invite");

     var to=tmpl.find('#enterEmail').value;
    Meteor.call('sendEmail', to);
      Session.set('email_invite',false);
     console.log(to);
  },
  'click .closeEmail':function(evt,tmpl){
    Session.set('email_invite',false);
  }
});
Template.showMembers.helpers({
  invited:function(){
    var project=Session.get('active_projects');
    console.log(project);
    //var projectname=project.name;
    //console.log(projectname);
    console.log(Invites.find({},{}).count());
    console.log(Invites.find({reciprocal:1,me:Meteor.userId()}).count());
    //console.log(Invites.find({reciprocal:1,me:Meteor.userId(),projectid:project,projectname:projectname,live:1}).count());
    return Invites.find({me:Meteor.userId(),reciprocal:1,live:1}).fetch();
  }
});
Template.showMembers.events({
  'click .closeSeeMembers':function(){
    Session.set('see_members',false);
  }

})
