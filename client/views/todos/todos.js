
Template.todos.helpers({
  todos: function(){
      console.log(Session.get('active_project'));
     return Todos.find({project:Session.get('active_project'),archived:{$ne:true}});
  }
});


Template.todos.events({
  'click .archive': function(event, template){
      //Session.set('attachScreenShots',true);
     Meteor.call('archiveTodo',this._id,!this.archived);
  },
  'click .todochecked':function(evt,tmpl){
    console.log(this._id);
    alert("Attach screenshot for verifications");
      Session.set('TaskId',this._id);
      console.log(Session.get('TaskId'));
      Session.set('attachScreenShots',true);
      console.log(Session.get('attachScreenShots'));
      this.completed=true;
      console.log(this.completed);
    Meteor.call('completeTodo',this._id,this.completed);
  },
  'click .addtodo':function(evt,tmpl){
    Session.set('adding_todo',true);
  }
});
Template.todoDlg.events({
  'click .saveTodo':function(evt,tmpl){
      var project=Session.get('active_project');
    var todo = {};
    todo.note = tmpl.find('.todoitem').value;
    todo.project = Session.get('active_project');
    todo.owner=Meteor.userId();

    todo.startDate=tmpl.find('#startDate').value;
    todo.endDate=tmpl.find('#endDate').value;
      todo.completed=false;
    todo.date=moment().calendar();
   todo.targetowner=tmpl.find('#taskMember').value;
      console.log(todo.targetowner);


      var guide=Projects.findOne({_id:project},{guidename:1}).guidename;
      console.log(guide);
      if(guide==todo.targetowner){
          alert("Cannot Assign Task To Guide");
      }
    if(todo.targetowner=="select"){
      todo.targetowner=Meteor.userId();
    }
      if(todo.startDate==new Date()){
          todo.gap=moment(todo.endDate).from(new Date());
      }
      todo.gap=moment(todo.endDate).from(todo.startDate);

    var targetname=Meteor.users.findOne({_id:todo.targetowner},{profile:{name:1}}).profile.name;
    console.log(targetname);
    todo.targetname=targetname;
    todo.archived = false;
    todo.completed = false;
    console.log(todo);

    var endTime=Date.parse(todo.endDate);
    var startTime=Date.parse(todo.startDate);
    var timediff=endTime-startTime;



    var seconds = ("0" + Math.floor( (timediff/1000) % 60 )).slice(-2);
    var minutes = ("0" + Math.floor( (timediff/1000/60) % 60 )).slice(-2);
    var hours = ("0" + Math.floor( (timediff/(1000*60*60)) % 24 )).slice(-2);
    var days = Math.floor( timediff/(1000*60*60*24) );
    var t={
      seconds:seconds,
      minutes:minutes,
      hours:hours,
      days:days
        }
      console.log(moment(todo.endDate).subtract(t.days/2,'days').calendar());

    var details={};
      details.from=Meteor.users.findOne({_id:Meteor.userId()},{profile:{name:1}}).profile.name;
      details.to=Meteor.users.findOne({_id:todo.targetowner},{profile:{name:1}}).profile.name;
      details.subject="TASK COMPLETED";
      details.date=todo.endDate;
      details.taskName=todo.note;
      details.text="Hello "+details.to+",Your task "+details.taskName+"  assigned to you is reaching " +
      "completion please report as soon as possible";
   //--->> Calling scheduleMail function to send emails
   Meteor.call('scheduleMail',details);

    if(todo.startDate==todo.endDate){
      alert("Start Date and End Date cannot be same");
    }
    else {
      Meteor.call('addTodo', todo);
        GlobalNotification.success({
            // title: 'New Project Created',
            content: "New task " +todo.note+"  has been created and assigned to"+todo.targetname ,
            duration: 5 // duration the notification should stay in seconds
        });
      //Meteor.call('scheduleMail',details,todo);

    }
    Session.set('adding_todo',false);
    Session.set('newTask',true);
  },
  'click .closeTodo':function(evt,tmpl){
    Session.set('adding_todo',false);
  }
});

Template.todoDlg.rendered = function(){
  $('.datetimepicker').datetimepicker();
}
